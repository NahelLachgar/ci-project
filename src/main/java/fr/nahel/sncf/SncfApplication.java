package fr.nahel.sncf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SncfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SncfApplication.class, args);
	}

}
