package fr.nahel.sncf.models;

import java.util.List;

public class TableauAffichage {
    private List<Departure> departures;

    public List<Departure> getDepartures() {
        return departures;
    }

    public void setDepartures(List<Departure> departures) {
        this.departures = departures;
    }
}
