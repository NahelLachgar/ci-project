package fr.nahel.sncf.models;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Train {
    private String destination;
    private int number;

    public Train(String destination, int number) {
        this.destination = destination;
        this.number = number;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

