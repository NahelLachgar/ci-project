package fr.nahel.sncf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Departure {

    @JsonProperty("display_informations")
    private DisplayInformation displayInformation;

    public DisplayInformation getDisplayInformation() {
        return displayInformation;
    }

    public void setDisplayInformation(DisplayInformation displayInformation) {
        this.displayInformation = displayInformation;
    }
}
