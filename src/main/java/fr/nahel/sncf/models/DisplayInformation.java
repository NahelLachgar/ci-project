package fr.nahel.sncf.models;

public class DisplayInformation {
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    private String direction;
}
