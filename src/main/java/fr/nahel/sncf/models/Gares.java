package fr.nahel.sncf.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class Gares {
    @JsonProperty("stop_areas")
    List<Gare> gares;

    public List<Gare> getGares() {
        return gares;
    }

    public void setGares(List<Gare> gares) {
        this.gares = gares;
    }
}
