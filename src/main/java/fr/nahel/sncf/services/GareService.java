package fr.nahel.sncf.services;

import fr.nahel.sncf.models.Gare;
import fr.nahel.sncf.models.Gares;
import fr.nahel.sncf.models.TableauAffichage;
import fr.nahel.sncf.models.Train;
import java.util.List;

public interface GareService {
    public TableauAffichage getTableauDeparts(String id);
    public Gares getGares();
}
