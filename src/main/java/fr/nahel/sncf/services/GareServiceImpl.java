package fr.nahel.sncf.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.nahel.sncf.cache.MemoryCache;
import fr.nahel.sncf.models.*;
import fr.nahel.sncf.repositories.GareRepository;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import fr.nahel.sncf.models.TableauAffichage;

import java.io.IOException;

@Service
@Primary
public class GareServiceImpl implements GareService {
    @Autowired
    private GareRepository repo;
    @Autowired
    private MemoryCache cacheGares;
    @Autowired
    private ObjectMapper mapper;

    @Value("${navitiaToken}")
    private String navitiaToken;

    /**
     *
     * @param url
     * @return Response : la réponse si tout va bien, null sinon
     */
    private Response apiRequest(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization","3b036afe-0110-4202-b9ed-99718476c2e0")
                .build();
        try {
            Response response = client.newCall(request).execute();
            return response;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Cacheable("gares")
    public Gares getGares() {
        System.out.println("cache");
        if (cacheGares.getCache() != null) {
            return (Gares) cacheGares.getCache();
        }
        Response response = apiRequest("https://api.navitia.io/v1/coverage/sandbox/stop_areas?count=1000&");
        try {
            Gares gares = mapper.readValue(response.body().byteStream(), Gares.class);
            repo.saveAll(gares.getGares());
            cacheGares.updateCache(gares);
            return gares;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Cacheable("tableauAffichage")
    public TableauAffichage getTableauDeparts(String idGare) {
        System.out.println("Perdu "+idGare);
        Response response = apiRequest("https://api.navitia.io/v1/coverage/sandbox/stop_areas/stop_area:"+idGare+"/departures?from_datetime=20200303T144459&");
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            TableauAffichage tableau = mapper.readValue(response.body().byteStream(), TableauAffichage.class);

            return tableau;
        }
        catch (IOException e) {
            System.out.println("Erreur réseau");
            e.printStackTrace();
            return null;
        }
    }
}