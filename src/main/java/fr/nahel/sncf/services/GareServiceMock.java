package fr.nahel.sncf.services;

import fr.nahel.sncf.models.Gares;
import fr.nahel.sncf.models.TableauAffichage;
import org.springframework.stereotype.Service;


@Service
public class GareServiceMock implements GareService {

    @Override
    public TableauAffichage getTableauDeparts(String id) {
        return null;
    }

    @Override
    public Gares getGares() {
        return null;
    }
}
