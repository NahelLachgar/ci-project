package fr.nahel.sncf.repositories;

import fr.nahel.sncf.models.Gare;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public interface GareRepository extends CrudRepository<Gare, String> {

}
