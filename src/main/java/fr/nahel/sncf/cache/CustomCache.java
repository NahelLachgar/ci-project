package fr.nahel.sncf.cache;

public interface CustomCache<T> {
    public T getCache();
    public void updateCache(T cache);
    public void invalidateCache();
}
