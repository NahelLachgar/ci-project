package fr.nahel.sncf.cache;

public class NoOPCache<T> implements CustomCache<T> {

    private T cache;

    @Override
    public T getCache() {
        return null;
    }

    @Override
    public void updateCache(T cache) {
        System.out.println("Cache inactif");
    }

    @Override
    public void invalidateCache() {
        System.out.println("Cache inactif");
    }
}
