package fr.nahel.sncf.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

public class DBCache<T> implements CustomCache<T> {

    @Autowired
    private CrudRepository repo;

    @Override
    public T getCache() {
        return (T) repo.findAll();
    }

    @Override
    public void updateCache(T cache) {
        repo.save(cache);
    }

    @Override
    public void invalidateCache() {
       repo.deleteAll();
    }
}
