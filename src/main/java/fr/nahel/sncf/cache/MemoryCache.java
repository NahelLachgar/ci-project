package fr.nahel.sncf.cache;

import org.springframework.stereotype.Service;

@Service
public class MemoryCache<T> implements CustomCache<T> {

    private T cache;

    @Override
    public T getCache() {
        return cache;
    }

    @Override
    public void updateCache(T cache) {
        this.cache = cache;
    }

    @Override
    public void invalidateCache() {
        cache = null;
    }
}