package fr.nahel.sncf.cache;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class FileCache<T> implements CustomCache<T> {

    private ObjectMapper mapper;

    @Override
    public T getCache() {
        return null;
    }

    @Override
    public void updateCache(T cache) {
        String className = cache.getClass().getName();
        File file = new File(className+".json");
        try {
            mapper.writeValue(file, cache);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void invalidateCache() {

    }
}
