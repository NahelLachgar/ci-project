package fr.nahel.sncf.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthcheckController {
    @GetMapping()
    public String check() {
        return "ok";
    }
}
