package fr.nahel.sncf.controllers;

import fr.nahel.sncf.services.GareService;
import fr.nahel.sncf.models.Gares;
import fr.nahel.sncf.models.TableauAffichage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gare")
public class GareController {

    @Autowired
    private GareService gareService;

    @GetMapping("/{id}")
    public TableauAffichage getGare(@PathVariable("id") String id) {
        return gareService.getTableauDeparts(id);
    }

    @GetMapping()
    public Gares getAllGares() {
        return gareService.getGares();
    }
}
